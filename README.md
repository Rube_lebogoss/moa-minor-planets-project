# Moa Minor Planets Scripts

Author: Benkadoum Yaniss (from codes previously conducted by Amelia Cordwell and Pierre Dell'Ova)

This README is inspired from Amelia's README page. Take a look at her codes which where the starting point
of my researches to complete this project:
	 https://bitbucket.org/cordwella/moa-minor-planets/src/master/

An intergrated repository for all of MOA minor planet software, includes tracking, 
ccd processing and difference imaging photometry and lightcurves analysis. Currently due to the python 2 requirements 
of iraf/pyraf and pyDIA as a whole repo this only runs on python 2.7 however most of the scripts
should also run under python3.

Tested on Ubuntu Linux (18.04) only.

## Functionality of this repository

This repository is capable of taking a known asteroid id, tracking it through MOA exposures (given INFO files),
extracting the exposures it is in from the MOA object store database, displaying these images,
performing CCD processing on them and then doing difference image photometry on the images, to generate a lightcurve
and to inverse it to perform an analyse of its shape, rotation period and axis. The lightcurve will then
be calibrated to cousins I magnitudes (This is an approximate calibration).
During processing and before generating
the difference images the frames will be split 512 (+ BORDER) x 512 ( + BORDER) subframes.

This will only track and perform photometry on galatic bulge R band exposures.

## Setting up this repository

To get full functionality out of the programs contained a number of setup steps are required.

Firstly clone this repo into a folder of your own choice

Then follow the pyDIA installation instructions to install pyDIA,
the astroconda enviroment and CUDA/pyCUDA.

With pip install into your astroconda enviroment: requests, boto2, ccdproc, astroquery, astropy, pyds9

Update your python path to include this repository. (E.G. 'export PYTHONPATH="$(pwd)"'
if you are currently inside this directory). This can also be done inside any scripts
which might use this code by using sys.path.append('/path/to/repo'))

Setup IRAF. pyDIA uses IRAF to find star positions and compute the initial PSF,
in each directory you call a script that class iraf functions run mkiraf (iraf can
then be accessed with cl, or within scripts).

### Modify the settings for your file path an moa database access credentials

A file called settings.py will need to be placed into the moa_asteroids directory,
this contains your access credentials for the MOA database. Using read only credentials is advised.

The file should look like this:
	PARPATH = '/home/ubuntu/Documents/finalcode/par'
```
# Data save path
DATAP = '/home/ubuntu/Documents/finalcode/data'

# Photometric Calibration File path
CALIBP = '/home/ubuntu/Documents/finalcode/photcalib'

PYDIA_DIR = '/home/ubuntu/Documents/pyDIA'
ACCESS_KEY = ''
SECRET_KEY = ''
BUCKET = 'moa'

at_uoa_campus = False

if at_uoa_campus:
   	S3_HOST = 'object.auckland.ac.nz'
else:
   	S3_HOST = 'objectext.auckland.ac.nz'
```

### Download Required Files

INFO files from the MOA database need to be downloaded locally before the
scripts in this repository can be run, and the data directory structure will
need to be setup.

To download the INFO file I advise using some of the scripts from here:
https://bitbucket.org/cordwella/moa-db-scripts, particulally download_list. 
The path in the MOA_DB to download those files from is: info/GB The file structure 
for these will need to then be flattened.

Reference images for difference photometry will need to be downloaded to DATAP/ref/.
These files are not yet publically avaliable, however Ian Bond has access to all of them. 
This folder will be updated with cropped copies of the reference frames for each subframe.

To include lightcurve calibration the moa photocalib files are also required. These files include 
instrumental magnitudes of possible reference stars, and a mapping from those instrumental magntiudes
to Cousins I magnitudes (calcluated from OGLE data).


## Parts of this repository

tracking/ 

An adapted part of Ian Bond's MOA asteriod tracking scripts, this will generate datafiles
tracking a given asteriod through MOA exposures and generate a list of exposures and where 
to find the asteriod in a given exposure.

Note that this set of codes adapts upon the orginal code to give x, y cooridinates in a specific 
subframe, this is appended on to the original generated output into the files.

processing/ 

Download flats and darks for a given moa exposure (goodness checking of theses is now automated) then process downloaded
fits files, also crops downloaded frames to subframes.

photometry/ 

Uses Micheal Albrow's pyDIA code to perform difference imaging photometry on images for an asteriod,
then it automatically checks and ensures that the difference imaging on each frame is correct and that the calculated 
position is correct. After this photometry can be extracted from each image, placed into a lightcurve and calibrated to cousins I magntiudes

analysis/

Uses NASA's HORIZONS codes to get positions at each epoch of the asteroids and DAMIT codes to the lightcurves inversion.
Scan the period to find the best rotation period and then find the best rotation axis parameters and rotation period. An estimation of the 
H-magnitude in the I-band is processed and thus a reconstruction of the asteroid shape is done. 

Note that all the codes are running and finishing but a last work of calibration verification and coordinates in the astrocentric reference
system is needed to ensure to have a the good result for the shape recontruction of the asteroid which is actually not totally the case.

### Data saving

Data will be saved to your setup DATAP in settings.py

Tracking data will be saved in DATAP/tracking/ Images and lightcurve output for an asteroid will
be saved in DATAP/asteroids/{asteroid_id}/. Inside this folder source/ contains the source images from the MOA DB,
processed/ contains the exposures with darks, flats and subframing applied and difference/ contains all of the output
from difference imaging from pyDIA. Generated files for each subframe/chip/field run will have {}{}{} appended to the
end of the name to ensure they are kept seperate. Additional files added to this folder are fullpmosaic.fits and fullmosaic.fits,
which are mosaics of all of the images of an asteroid with the PSF removed and not respectively. positions.dat contains the human
calibrated position for each asteroid from the check_images.py (previously positions.py) script. lightcurve.png will be a copy
of the lightcurve from analysis/lightcurve_graphs.py.

difflightcurve.csv and difflightcurve_cal.csv both have the same file format. The columns from left to right are:
	Time mid exposure (Julian days), frame name, flux in photometric counts, flux error, MOA instrumental magnitudes, MOA instrumental magnitude error, I magnitude, I magnitude error

An adaptation of those files is needed to use DAMIT inversion codes. The new format of those adaptations saved in DATAP/asteroids/{asteroid_id}/HORIZONS/horizons_lightcurve.csv is:

The input file contains lightcurve data and the corresponding geometry, it is read from the
standard input. The first line gives the total number of lightcurves (all points taken in the same epoch are considered from the same lightcurve),
then the individual lightcurves follow in ‘blocks’. Each lightcurve starts with the number of points and 0/1 code
for a relative (0) or calibrated (1) lightcurve. Then there are lines with the epoch in JD
(light-time corrected! ), the brightness in intensity units (reduced to unit distances from the
Earth and the Sun when calibrated), the ecliptic astrocentric cartesian coordinates x, y, z of
the Sun and of the Earth in AU.
For a slowly moving main belt asteroid, the coordinate vectors can be approximated as
to be constant for a single-night lightcurve.

The shape reconstruction file and others usefull files for the DAMIT inversion are saved in DATAP/asteroids/{asteroid_id}/HORIZONS


## Example Usage

### Tracking an asteroid

Tracking an asteroid is almost the exact same as in Ian Bonds original code,
seen here https://bitbucket.org/iabond/asteroids. I advise using his codes example.

If you are wishing to track a number of asteroids I advise using the batch script. 
Simply edit the bottom of the file to include the asteroid IDs you care about.

To manually track a single asteroid first generate the empheridies and then track 
it through the field
```
cd moa_asteroids
python tracking/make_ephem.py {asteroid_id}
python tracking/track_in_moa.py {asteroid_id}
```
The tracking file will be saved as DATAP/asteroids/{asteroid_id}/track.dat

The track can then be viewed with
```
python trackng/draw_track.py {asteroid_id}
```
### Generating Statistics for asteroids

After tracking a single, or a number of asteroids statistics around how many frames that it appears in
and the number of passes. Run
```
python stats.py {asteroid_id}
```
will print out the statistics for that asteroid and save
it to a file (DATAP/asteroids/{asteroid_id}/stats.txt).

The batch stats command can be run from the batch.py file, and will generate the same statitics 
but also compute the average statistics for the entire list of asteroid ids passed into the function.


### Downloading passes that an asteroid appears in

Prerequsites: Completed tracking of the asteroid

This will download all source images of the asteroid.
```
cd moa_asteroids
python download_frames.py {asteroid_id}
```
The files will be saved in DATAP/asteroids/asteroid_id/source


### Running CCD Processing

Prerequsites: CCD frames for an asteroid downloaded to data/asteroids/{asteroid_id}/source Flat and dark
info files saved to data/info. Python enviroment with astroconda and boto setup.

Will output processed images to asteroids/{asteroid_id}/processed, and write to darks.dat matching dark images
and to flats.dat matching flat images. Master dark and flat images will be created in data/dark_master and data/flat_master.
Flat and dark images will also be downloaded to data/dark and data/flat respectively these directories can be cleared afterwards.

Create a few nessecary directories and files
```
touch data/darks.dat
touch data/flats.dat

mkdir data/dark
mkdir data/dark_master
mkdir data/flat
mkdir data/flat_master

mkdir data/asteroids/{asteroid_id}/processed
```
Add this directory to your python path, change directories to moa_asteroids/processing
then python 
```		
essai_process.py {asteroid_id}
```
(previoulsy process {asteroid_id}).

#### Subframes

Each downloaded image will be cropped into a subframe of 512 x 512 pixels, with aside from subframes at the edge
of the original frame will also have a a border added incase the asteroid is on the edge of a frame. 
The subframe names take the form of '{row}_{column}' with both numbers starting at zero and starting from the bottom
left of the frame.

The subframe size can be changed in constants.py, however if this occurs it is advisible to also update the subframe
naming in util.py.

### Viewing images that an asteroid appears in

Prerequsites: Downloaded images and performed tracking and ccd processing on the asteroids
```
python view_in_ds9.py {asteroid_id}
```
## Generating difference images for an asteorid

Prerequsites: Source images have basic ccd processing applied to them and tracking exists
```
python photometry/essai_make_difference_images.py d {asteroid_id}
```

The difference images and all of their outputs will be placed into DATAP/asteroids/{asteroid_id}/difference.
See the pyDIA documentation for details on what these files contain. If an odd c error occurs on the set of 
images (eg an error with free) this is an error with pyDIA and the reference image. To process the rest of the asteroid 
exposures remove all of the images for that subframe/chip/field from the processed directory, and then all of the pyDIA
generated files from the difference directory.


### View generated difference images and generate a list of asteroid positions

Due to slight differences in pointing and uncertainty in asteroid positions the calculated position of the asteroid
from the empheridies may not be the exact position of the object. Addionally poor quality images, or images where
the minor planet is next to a saturated star will need to be removed from the set used to calculate the lightcurve.

One the difference images are calculated, run 
```
python photometry/check_images.py {asteroid_id}

```
which will bring up ds9 and display side by side the source image and the difference image and check autmatically if
the crosshair on the difference image is on the centre of the asteroid and if the difference images are good enough to be kept (not too noisy ...).
The parameter of star detection and acceptation rate could be changed depending on the subframe. It could be an issue for a future student working on this project.
If you get an error and ds9 doesn't pop up you may need to download the ds9 exceutable and start it yourself.

### Perform photometry on the asteroid

Prerequsites: Difference images have been generated and a positions file has been created
```
python photometry/essai_make_difference_images.py p {asteroid_id}
```
The lightcurve will be saved in data/asteroids/{asteroid_id}/difflightcurve.csv.
This has the format julianday, frame, flux, fluxerr, instrumental mag, instrumental mag error, I band mag, I band mag error

If not enough valid comparison stars are avaliable the magnitudes will not be calculated. 
Valid comparison stars are generated by comparing the predicted uncertainty of the star with the 
difference flux from pyDIA, reasons for bad comparison stars are typically poor difference imaging,
poor intial images (rotator offset, clouds), or stellar variablity.

This will also generate in the difference directory fullmosaic.fits and fullpmosaic.fits displaying cutouts 
of the asteroid in the difference images, before and after the psf has been fitted to it respectively.

### Calibrate the photometry on the asteroid

pyDIA will photometrically scale images to the reference but different references will have a different base 
of photometry. This process is a part of the standard photometry computation by default, but can also be done on it's own.

To setup the photometry calibration the MOA-Red to Cousins I calibration coefficents need to be in
the photcalib directory, with the structure:
	photcalib/gb{}/moa-ogle-gb3-R-2.coef

```
python calibrate_lightcurve.py {asteroid_id}
```

### Analyse the lightcurve of an asteroid

Prerequsites: lightcurves are calibrated and the asteroid has enough points in its lightcurve to hope have reliable results on the rotation
period (basically minimum 30/40 points are needed, below this the analysis won't be reliable and convergent).

To find with the simulated annealing method the best rotation period by wrapping the time of the lightcurve run
```

cd analysis
python essai_horizons.py {asteroid_id}
```
It will show the eight wrapped lightcurves and their period, process period_scan code from DAMIT package and try to minimize
chi squarre fonction around the first estimation of the roation period.
Then enter the minimum and maximum rotation period which (seem to) form a parabol. A second degree polynome is fitted on it 
and the rotation period corresponding to the minimim of this one is used to process DAMIT codes to find the best rotation axis parameters.

Thus it runs minkowski asteroid shape reconstruction code and writes the results in DATAP/asteroids/{asteroid_id}/HORIZONS/asteroid_shape file.
It has format :
	the first line gives the number of vertices and facets, then follow the vertex x, y, z
coordinates, then for each facet the number of vertices and the order numbers of facet vertices
(anticlockwise seen from outside the body).

To get a 3D plot of the asteroid shape run
```
python essai_plot_asteroid_shape.py {asteroid_id}
```

Note that all the codes are running and finishing but the shape result is not yet good. A verification of the calibration is according
to me needed (do they need to be calibrated ? because the not calibrated are supposed to get better results according to the DAMIT 
Readme file )  and also a verification of the astrocentric coordinates of the Sun and the Earth.



### Get the status of an asteroid

The statuses are as follows

- No frames (in tracking) NO_MATCH
- Tracked frames no images MATCH_NO_IMAGES
- Images downloaded SOURCE_IMAGES
- Images have preprocessing PROCESSED_IMAGES
- Difference imaging carried out HAS_DIFFERENCE_IMAGES
- Photometry Carried out INITIAL_LIGHTCURVE
- Photometry and Calibration Carried out CALIBRATED_LIGHTCURVE
- Post analysis done ANALYSIS_COMPLETE (codes implemented but not yet the status)

Assumes that if any files for images exist that all of that type of image exists

Either run 
```
python status.py 
```
to get the status and number of visited frames for all asteroids that an ephem has been calculated


### View the lightcurve graph (I magnitudes)

If the period is unknown
```
python analysis/lightcurve_graphs.py {asteroid_id}
```
Timewrapped with a known period (in hours)
```
python analysis/lightcurve_graphs.py {asteroid_id} {period}
```
## Batch run a number of asteroids

The batch.py script can be used to run tracking, downloading, processing and difference 
imaging for a set of asteroid ids, as well as create graphs about the group statistics of those asteroids.
Currently the file must be edited to do anything different with it. Simply go to the bottom 
of the file and change the asteroid id list and modify the true false values for each of the options 
in the batch command, then run it as a python command.

One can run automatically the tracking, downloading and processing codes for a bunch of asteroids, to do that run
```
cd moa_asteroids
python all_processing a {starting_asteroid_id} {ending_asteroid_id}
```
It will check if there is enough points in the tracking file and enough points in the processed images to hope 
have a good lightcurve inversion. The potential good asteroid ids are saved in DATAP/already_processed_asteroids.txt and
those which are not interesting to be inversed are saved in DATAP/bad_asteroids.txt.

If you want to run only some asteroids type
```
python all_processing {asteroid_id_1} {asteroid_id_2} ... {asteroid_id_n}
```

Note that in the all_processing code a part is already implemented to check if there is enough points after that the phometry was processed
on the asteroid but the issue is that it opens each time a new ds9 viewer and according to its Harvard documentation a ds9 viewer
needs to be closed manually. So, it can be an isue for a futur student to fix this problem to process all the codes automatically whitout
a ds9 viewers spam.